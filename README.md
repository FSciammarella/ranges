# Knuth–Morris–Pratt

Various functions using the Knuth–Morris–Pratt algorithm to efficiently find patterns.